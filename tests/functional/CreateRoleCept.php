<?php
$I = new FunctionalTester($scenario);

$I->am('admin');
$I->wantTo('create a new role');

// log in as your admin user
// This should be id of 1 if you created your manual login for a known user first.
Auth::loginUsingId(1);

// When
$I->amOnPage('/admin/roles');
$I->see('Roles', 'h1');
$I->dontSee('Randomtest');
// And
$I->click('Add Role');

// Then
$I->amOnPage('/admin/roles/create');
// And
$I->see('Add Role', 'h1');
$I->submitForm('.addrole', [
    'name' => 'Randomtest',
]);
// Then
$I->seeCurrentUrlEquals('/admin/roles');
$I->see('Roles', 'h1');
$I->see('Randomtest');
