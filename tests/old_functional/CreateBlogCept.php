<?php
$I = new FunctionalTester($scenario);

$I->am('an author');
$I->wantTo('create a new blog');

// When
$I->amOnPage('createblog');

// Then
$I->seeCurrentUrlEquals('/createblog');
$I->see('Create Blog', '.title');
