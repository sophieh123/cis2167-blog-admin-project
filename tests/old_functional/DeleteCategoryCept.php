<?php
$I = new FunctionalTester($scenario);

$I->am('an editor');
$I->wantTo('delete a category');

// When
$I->amOnPage('category');

// Then
$I->seeCurrentUrlEquals('/category');
$i->see('Category', '.title');
$I->click('button[type=submit]');
