-- MySQL dump 10.13  Distrib 5.7.20, for Linux (x86_64)
--
-- Host: localhost    Database: blogadmin
-- ------------------------------------------------------
-- Server version	5.7.20-0ubuntu0.17.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `article_category`
--

DROP TABLE IF EXISTS `article_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `article_category` (
  `article_id` int(10) unsigned NOT NULL,
  `category_id` int(10) unsigned NOT NULL,
  KEY `article_category_article_id_index` (`article_id`),
  KEY `article_category_category_id_index` (`category_id`),
  CONSTRAINT `article_category_article_id_foreign` FOREIGN KEY (`article_id`) REFERENCES `articles` (`id`) ON DELETE CASCADE,
  CONSTRAINT `article_category_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `article_category`
--

LOCK TABLES `article_category` WRITE;
/*!40000 ALTER TABLE `article_category` DISABLE KEYS */;
/*!40000 ALTER TABLE `article_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `articles`
--

DROP TABLE IF EXISTS `articles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `articles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `author_id` int(10) unsigned NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `published_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `articles_title_unique` (`title`),
  KEY `articles_published_at_index` (`published_at`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `articles`
--

LOCK TABLES `articles` WRITE;
/*!40000 ALTER TABLE `articles` DISABLE KEYS */;
INSERT INTO `articles` VALUES (1,'Tempora optio inventore numquam sed.','Amet facilis deleniti et et veritatis voluptas doloremque. Esse illo magni eaque. Ullam ipsum adipisci voluptas eligendi veniam. Perferendis temporibus quisquam ea ullam.','cupiditate',1,'2018-02-28 10:48:22','2018-02-28 10:48:22','0000-00-00 00:00:00'),(2,'Voluptas eos odio odio tempore aut voluptates ipsam.','Dolorem vitae error eveniet est. Deleniti cumque deserunt praesentium iusto ducimus veniam enim. Velit consectetur quibusdam ut nihil. Sint unde soluta nisi dolorum esse.','ut',1,'2018-02-28 10:48:22','2018-02-28 10:48:22','0000-00-00 00:00:00'),(3,'Quis quis dolores explicabo minus aut accusamus natus.','Autem consequatur quibusdam quisquam. Quisquam alias ducimus dolores omnis itaque. Pariatur itaque qui perferendis voluptatem. Laboriosam animi sed laudantium doloribus et et dignissimos.','molestias',1,'2018-02-28 10:48:22','2018-02-28 10:48:22','0000-00-00 00:00:00'),(4,'Vero sed quae rerum fuga ad ea.','Molestias possimus quam consequuntur ut. Dolor nostrum consequatur voluptatem mollitia quam. In et eius necessitatibus iusto ipsam architecto ut. Vero et consequatur reprehenderit aut minima sed.','aut',1,'2018-02-28 10:48:22','2018-02-28 10:48:22','0000-00-00 00:00:00'),(5,'Dolore eius consequuntur consequatur consequatur.','Illo commodi quia voluptas officiis dolores necessitatibus tempora. Repellat aut nemo nobis eum dolor commodi totam. Consequatur et ut ut hic voluptatem et dolor.','sequi',1,'2018-02-28 10:48:22','2018-02-28 10:48:22','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `articles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `detail` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `categories_title_unique` (`title`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,'HTML','html',NULL,NULL),(2,'PHP','php',NULL,NULL),(3,'CSS','css',NULL,NULL);
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES ('2014_10_12_000000_create_users_table',1),('2014_10_12_100000_create_password_resets_table',1),('2016_02_19_100050_create_roles_table',1),('2016_02_19_100108_create_permissions_table',1),('2016_02_19_100123_create_articles_table',1),('2016_02_19_100137_create_categories_table',1),('2016_02_19_100200_create_role_user_table',1),('2016_02_19_100219_create_permission_role_table',1),('2016_02_19_100236_create_article_category_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permission_role`
--

DROP TABLE IF EXISTS `permission_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permission_role` (
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `permission_role_role_id_foreign` (`role_id`),
  CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permission_role`
--

LOCK TABLES `permission_role` WRITE;
/*!40000 ALTER TABLE `permission_role` DISABLE KEYS */;
/*!40000 ALTER TABLE `permission_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `permissions_name_unique` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions`
--

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_user`
--

DROP TABLE IF EXISTS `role_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_user` (
  `role_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`role_id`,`user_id`),
  KEY `role_user_user_id_foreign` (`user_id`),
  CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_user`
--

LOCK TABLES `role_user` WRITE;
/*!40000 ALTER TABLE `role_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'sophie','sophie@example.com','$2y$10$/2sxjBEr14eynVhmQiBuAePI3PJryUBcjGO2qyXwuDxJ6qYl6o3dq','jqZAkMhAAj',NULL,NULL),(2,'Mr. Douglas Goodwin','wgulgowski@example.com','$2y$10$.Lyy0v.0mwGKEvYNWC/2mepuJYi79Dbd4coPSjxqeadEdcfAFde8C','fAI4oKZxbn','2018-02-28 10:48:21','2018-02-28 10:48:21'),(3,'Eda Moore DDS','blair.harber@example.com','$2y$10$95ZqRuO6Wxo41s6IRQqfJewGVrfB.asWA6dkeeIHVOQCeTmaYxyQO','Hd9d1y9ZKp','2018-02-28 10:48:22','2018-02-28 10:48:22'),(4,'Dr. Margaretta Lehner','hodkiewicz.justice@example.org','$2y$10$KBDj29HM0p.8GKuG50Etwe1MGeDZceFsPAVsJ8zxMTE8rNRnJF3m2','352p9t0YhZ','2018-02-28 10:48:22','2018-02-28 10:48:22'),(5,'Nigel Lemke','kemmer.peggie@example.org','$2y$10$q0i7fZICuz7H7nCz8rB11OvtArHe1VdS6XjUr5fvvkoJCuqZrkXHG','5dDk3kfeaF','2018-02-28 10:48:22','2018-02-28 10:48:22'),(6,'Billy Beier','johnathan92@example.org','$2y$10$npUMVYZLChp0CQ.ugNUE3exjgm0pdfU2AFJ/JBNkVR4k9c/guyGJK','uWFkKKEsIp','2018-02-28 10:48:22','2018-02-28 10:48:22'),(7,'Helene Little','xwolff@example.com','$2y$10$mhEbp.TuEhb02Ea8hQUqbuwuaRlfVHMXdPYxb1psPL8A7PukLpdj6','ybXRD2tDq4','2018-02-28 10:48:22','2018-02-28 10:48:22'),(8,'Prof. Alysa Ziemann','upton.freeman@example.com','$2y$10$YU6EpWW/bhR610lWDKS.VuI4HnmFSRgFzbSF9bCo8WZTI.Kg1n8X.','dQMHJLEldz','2018-02-28 10:48:22','2018-02-28 10:48:22'),(9,'Russ Torphy','americo96@example.org','$2y$10$oQQGdgMdC38bzfubjGWQTu..aZbqZbKV9bv4II24Ahf2ZcRrJ.Au.','Gb3hn8njrO','2018-02-28 10:48:22','2018-02-28 10:48:22'),(10,'Walton Rempel','little.estelle@example.org','$2y$10$GZeE66xiH9jUJDlIvn0zaeIfssBMsoAWPdqx4g0Dn1opxpeDekJHm','cfq9wZdfCo','2018-02-28 10:48:22','2018-02-28 10:48:22'),(11,'Etha Wyman PhD','koconner@example.org','$2y$10$LNvEf57jw5h1NOfLJZn93O82FbZEKrN9gd1hYV9K8MMBSy8.JH8vi','RDXvr1Wgdb','2018-02-28 10:48:22','2018-02-28 10:48:22'),(12,'Mr. Kristopher Larkin','lcormier@example.org','$2y$10$cN49MstB3fQHBrRT5SOzJOyo9N.Rbd7xHfwCBywbQlu4K1MTnQpS6','AEZdwktZHu','2018-02-28 10:48:22','2018-02-28 10:48:22'),(13,'Marlin Lubowitz','ddaniel@example.net','$2y$10$VWoy2OD3KlFP/7CZVOWQCe8WJ6..RHjsELHwMWsLR5CPV3kwHcXHC','jP0LgZ9ECL','2018-02-28 10:48:22','2018-02-28 10:48:22'),(14,'Chasity Murazik','npaucek@example.net','$2y$10$bBVCu1ohNfW3W/BICsEo6OIVTr7NfRfsbc1X9yOZZoh2sVunSlp1S','S0JUtdJG9N','2018-02-28 10:48:22','2018-02-28 10:48:22'),(15,'Martina Auer','dratke@example.org','$2y$10$7RIuE1DUToBG3aUqXZRf0ONbWFL4lEj0zHneUIp3.xWe0z2jrgXtC','ZMJTkXf0eg','2018-02-28 10:48:22','2018-02-28 10:48:22'),(16,'Briana Hayes','mstamm@example.org','$2y$10$zlXc0S/tD95O85IHW/2yFOA9DLm9kIDJCq0TlhQWYC35xnvOJXWkq','zVr9ClHM2I','2018-02-28 10:48:22','2018-02-28 10:48:22'),(17,'Prof. Eric Herzog','langworth.randall@example.com','$2y$10$tPHi50KYlEFGJOqPLGRXJe9aLysn8GzXUelCID8vZ2KUiZkGdRHcK','4jIecNuRqW','2018-02-28 10:48:22','2018-02-28 10:48:22'),(18,'Dr. Danny Koelpin MD','langworth.jesus@example.com','$2y$10$CH6CFA8mMF4qxCQLOlMmbuBF0FxcDDQEuyYDeLydZ/lhRkW4vyBYS','h4KSarQeur','2018-02-28 10:48:22','2018-02-28 10:48:22'),(19,'Rosie Mante','jkoepp@example.com','$2y$10$8tIrXtx9GRWTiRfWB120kuYP8UzegEVZ3Pn1j/c/akLFHtS/JtdHW','g1KJG6SPaV','2018-02-28 10:48:22','2018-02-28 10:48:22'),(20,'Andreanne Klein','kyleigh.breitenberg@example.net','$2y$10$JgKdg9OSDLdb8hKDTVMbnOyYInJ3zCsMolZAShBHpfeiDBlC8V.Dy','tBM6XbKGMU','2018-02-28 10:48:22','2018-02-28 10:48:22'),(21,'Susana Leuschke','garett.koch@example.org','$2y$10$RRMFTiGFGX1ofxdhzeWvberEOmB1ChLrxQpML3lz0QWvoB8FTcl6m','vf9EQOfYiT','2018-02-28 10:48:22','2018-02-28 10:48:22'),(22,'Eleanora West','runte.tanya@example.com','$2y$10$.oGCKE.Qi9vFARnw6i2R/OxNkC.Lp942U9TUf3ilcCy5lAnv.RneO','YWxDGS3l4X','2018-02-28 10:48:22','2018-02-28 10:48:22'),(23,'Otha McClure','mac28@example.net','$2y$10$gwjVaHYqPS.yC/zQSA/ck.UnO0/txksel1KIN6SD5zb2xrrMJBRge','z7o8zns7F9','2018-02-28 10:48:22','2018-02-28 10:48:22'),(24,'Waylon Padberg','stanley85@example.org','$2y$10$4m18Rvrc4Cths9s9t/nNnOKTPHFRXiv4UMi1bSfFGUzpcR3UTqEB2','5Dik34XlO3','2018-02-28 10:48:22','2018-02-28 10:48:22'),(25,'Fleta Schimmel','armstrong.jackie@example.net','$2y$10$J1AeOxTjj.MWcNtTfeu1neTzvt7EhmeWxkMWwr4PFef7zRdsycmbi','kv1Xtq2l5i','2018-02-28 10:48:22','2018-02-28 10:48:22'),(26,'Verdie Wisoky','maribel.bogisich@example.org','$2y$10$vEu1Vsem7WT9Z4vAygJvx.GFWie0mkuK8CzEjiUNSjjN1teyF81TO','wKknSPTpnX','2018-02-28 10:48:22','2018-02-28 10:48:22'),(27,'Damien Considine','joy.stiedemann@example.net','$2y$10$m2omItPFuaJev7vJe9FO8.BLh5ROE3q2KkHHp/01B5YFNHwthv9Ou','pbF9RwmKYT','2018-02-28 10:48:22','2018-02-28 10:48:22'),(28,'Erwin West DDS','borer.joanie@example.com','$2y$10$E8aJt35dg0/4lT7tQIbr0OVwIkRAeHOAm0ZrLW.RzQaM7h.iW/Wh2','A4OPJoBeZN','2018-02-28 10:48:22','2018-02-28 10:48:22'),(29,'Dr. Durward Hegmann I','levi.kunze@example.net','$2y$10$FDIw81b5rUTMjnj.D9w/deto5.8slmEdE7vRkQFWFg0LfOFN5F9Yu','56wU4BthpB','2018-02-28 10:48:22','2018-02-28 10:48:22'),(30,'Stewart Wehner','vonrueden.marcelle@example.com','$2y$10$.RjPWf9yRlQrYzWcH/5nEOc64WtJCFIh8YurfssM5kYlzS4uaDvsu','WM5iH79xTP','2018-02-28 10:48:22','2018-02-28 10:48:22'),(31,'Cayla Kunze','esteban06@example.com','$2y$10$CfE3dlkS1Dczlmo7QlXY5eXvuC/fMr3mTkJTnRTGUQ89fy7BvlRb.','pCt4IjkguR','2018-02-28 10:48:22','2018-02-28 10:48:22'),(32,'Dr. Gerhard Emard IV','andy.okeefe@example.com','$2y$10$kXcYjIzZ5k0ssCB.1gzTouaDQjhO22v8O15m2yFcXeHbZxQaMss3e','9zgoeQmX2q','2018-02-28 10:48:22','2018-02-28 10:48:22'),(33,'Cayla Christiansen','karley.terry@example.net','$2y$10$PWjkUTt8F3CH//S3H/iwM.DRlcptVQuzXVEGf0DjBQwb/EGOTUGjq','VfEnKYRXiL','2018-02-28 10:48:22','2018-02-28 10:48:22'),(34,'Barry Considine','maude.oconnell@example.org','$2y$10$TOsslJ1RabYBjkDHyU0zW.XH6oidnEbP0tK0juD64Ue66eR3knfli','LpWgC6kskf','2018-02-28 10:48:22','2018-02-28 10:48:22'),(35,'Dr. Granville Dickens','feest.tyler@example.com','$2y$10$oqI58uK2G7IDYSulFsy0a.ynntZrA0RYjc4zDfmD6X./VrfxSpfua','rcakecavVt','2018-02-28 10:48:22','2018-02-28 10:48:22'),(36,'Harley Mueller','lang.maymie@example.net','$2y$10$JTwk7Ev8VLCintvZGjogWuJdZqTxC2LZISFeDYzSbtVcH4sDftZpe','K1JLdErbUj','2018-02-28 10:48:22','2018-02-28 10:48:22'),(37,'Mrs. Adele Nikolaus II','abuckridge@example.org','$2y$10$7aPNAVFIyQkTKBaEXzwuRe8VZTuvt6Osp4yIGDNOVE5r9AszmAF/W','SQYTgzBXdv','2018-02-28 10:48:22','2018-02-28 10:48:22'),(38,'Darlene Huels','boyer.rossie@example.com','$2y$10$BErmJ37oRYF2P.P8NhC0HOE6EPYg46..n1ISFliCRgkMQkACqRADy','lVwYoUZpXd','2018-02-28 10:48:22','2018-02-28 10:48:22'),(39,'Dr. Annabel Bins','kruecker@example.com','$2y$10$QUEYYjMKRmdjHETkvUScJeRX6Lt.oM.O.yVN6c21rd0b2qUjCrKxS','93t5knyYV1','2018-02-28 10:48:22','2018-02-28 10:48:22'),(40,'Domenico Borer Sr.','lmorar@example.org','$2y$10$rgPVfHzs5fPSDtxQC8DMSulp1FlDVBLLywiwQOQQsEX/QICjhqT8G','XDbsqlHOep','2018-02-28 10:48:22','2018-02-28 10:48:22'),(41,'Mr. Orville Beahan IV','julien.keeling@example.org','$2y$10$v9qhF1zGqN5zYC54YMYQDudCwQsktbFfYEviIsCSEkc0OMNpemqzu','CnuSGSpjiJ','2018-02-28 10:48:22','2018-02-28 10:48:22'),(42,'Price Hand MD','qrunte@example.org','$2y$10$OHnP.G8IqpaYymuExXm9VOpnwEos4BbXopFmFTVShm/HnaFyKf1zu','7FxssB6OVG','2018-02-28 10:48:22','2018-02-28 10:48:22'),(43,'Maxine Kihn','schneider.cordie@example.net','$2y$10$LzLOexJQkPmUORwYuavQX.mbz4npNVqrKp4K9c5SdqFAk7t8C/Xt2','vOq8OA9Vyw','2018-02-28 10:48:22','2018-02-28 10:48:22'),(44,'Prof. Milford Spinka','garnett56@example.net','$2y$10$PccE4Oe8yGRQDUIRn93EnuDWnKWLWV0HYC9z9YdmuAY.PYuInkNNC','2N4NtayWZt','2018-02-28 10:48:22','2018-02-28 10:48:22'),(45,'Cassandra Bahringer','rozella.stehr@example.net','$2y$10$ayp9dIIdEm2yr6GUwo3v5eL7zXca9sbvDfxuIypyStmsWcwk5S7lm','i1GCamXId7','2018-02-28 10:48:22','2018-02-28 10:48:22'),(46,'Laurel Green','alexandrea00@example.org','$2y$10$GSctpbInS7AJ3ZwCqvBUDe5tTYrLdOJ6H5y6CjFpAsg6sS71.T6OO','mHHKKFAhl8','2018-02-28 10:48:22','2018-02-28 10:48:22'),(47,'Mr. Devyn Kub','violette.cronin@example.net','$2y$10$ipMzIIApvoYp5SV1Py1vueiSQrVfja8wbcyrxZtO9QZyD4zVQR8Sm','HgkreArqSn','2018-02-28 10:48:22','2018-02-28 10:48:22'),(48,'Marian Hilll III','bessie86@example.com','$2y$10$GUnenZGRUTaDuR9EQ2sjxuYoumTFqVt.dEBdfH9ncQq.WuyovjO16','ilXJHrhMVB','2018-02-28 10:48:22','2018-02-28 10:48:22'),(49,'Estel Jaskolski DVM','wilton80@example.net','$2y$10$uGJeFMZ1mcpQlHCAYVZ9OuDshkCkwJZMVqdDQEriz5U4ZBe6y3IdW','LELtwtupZF','2018-02-28 10:48:22','2018-02-28 10:48:22'),(50,'Gwen Reichert','tyost@example.net','$2y$10$U6Zn7dE96VZ4yHiwWZhJfOz1RImb2gB0xES07SIIP0KJ9B1/ysDj.','TYHGfaXsDw','2018-02-28 10:48:22','2018-02-28 10:48:22'),(51,'Brad Weimann','schuppe.vincenza@example.com','$2y$10$6.WZy.dcLKoqyoeW6hltD.E6z1O2WtPlmfo8lZJBcIsnxNJYU856q','cvuPL5pZCn','2018-02-28 10:48:22','2018-02-28 10:48:22');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-02-28 10:51:50
