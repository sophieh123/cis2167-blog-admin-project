<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>{{ $role->title }}</title>
</head>
<body>
<h1>{{ $role->title }}</h1>
<p>{{ $role->content }}</p>
</body>
</html>
