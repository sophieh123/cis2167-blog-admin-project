<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>All Users</title>
  <link rel="stylesheet" href="/css/app.css" />
</head>
<body>
  @can('see_adminnav')

      @include('admin/includes/adminnav')

  @endcan

  <h1>All Users</h1>

  <section>
    @if (isset ($users))

      <table>
        <tr>
          <th>Username</th>
          <th>Email</th>
          <th>Permissions<th>
        </tr>
        @foreach ($users as $user)
          <tr>
            <td><a href="/admin/users/{{ $user->id }}/edit" name="{{ $user->name }}">{{ $user->name}}</a></td>
            <td> {{ $user->email }} </td>
            <td>
                @foreach ($user->roles as $role)
                  <li>{{ $role->label }}</li>
                @endforeach
            </td>
          </tr>
        @endforeach
      </table>
    @else
      <p> no users </p>
    @endif
  </section>

</body>
</html>
