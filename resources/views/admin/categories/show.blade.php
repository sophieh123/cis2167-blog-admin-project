<!doctype html>
  <html>
  <head>
      <meta charset="UTF-8">
      <title>Admin - Categories</title>
      <link rel="stylesheet" href="/css/app.css" />
  </head>
  <body>
  <div class="container">
      <article class="row">
          <h1>Categories</h1>
          <div class="col-md-6">
            <a href="categories/create" class="btn btn-lg btn-success pull-right">Add Category</a>
          </div>
          <p>New category added!</p>
          <section>
              @if (isset ($categories))

                  <table class="table table-striped table-bordered">
                      <thead>
                      <tr>
                          <td>Category</td>
                          <td>Detail</td>
                          <td>Update</td>
                      </tr>
                      </thead>
                      <tbody>
                      @foreach ($categories as $category)
                          <tr>
                              <td>{{ $category->title }}</td>
                              <td>{{ $category->detail }}</td>
                              <td> <a name="{{ $category->id }}" href="categories/{{ $category->id }}/edit" class="btn btn-warning">Update</a></td>
                          </tr>

                      @endforeach
                      </tbody>
                  </table>
              @else
                  <p> No categories added yet </p>
              @endif
          </section>
      </article>
  </div><!-- close container -->

  </body>
  </html>
