<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Create Category</title>
    <link rel="stylesheet" href="/css/app.css" />
</head>
<body>
  <h1>Add Category</h1>

  {!! Form::open(array('action' => 'CategoryController@store', 'class' => 'createcategory')) !!}
        {{ csrf_field() }}
    <div class="row large-12 columns">
        {!! Form::label('title', 'Title:') !!}
        {!! Form::text('title', null, ['class' => 'large-8 columns']) !!}
    </div>

    <div class="row large-12 columns">
        {!! Form::label('detail', 'Details:') !!}
        {!! Form::textarea('detail', null, ['class' => 'large-8 columns']) !!}
    </div>

    <div class="row large-4 columns">
        {!! Form::submit('Add Category', ['class' => 'button']) !!}
    </div>
{!! Form::close() !!}
</body>
</html>
