<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Admin - edit category</title>
    <link rel="stylesheet" href="/css/app.css" />
</head>
<body>
  <h1>Edit category - {{ $category->title }} </h1>

  {!! Form::model($category, ['method' => 'PATCH', 'url' => 'admin/categories/'. $category->id]) !!}
        {{ csrf_field() }}
    <div class="row large-12 columns">
        {!! Form::label('title', 'Title:') !!}
        {!! Form::text('title', null, ['class' => 'large-8 columns']) !!}
    </div>

    <div class="row large-12 columns">
        {!! Form::label('detail', 'Details:') !!}
        {!! Form::textarea('detail', null, ['class' => 'large-8 columns']) !!}
    </div>

    <div class="row large-4 columns">
        {!! Form::submit('Update Category', ['class' => 'button']) !!}
    </div>
{!! Form::close() !!}
</body>
</html>
