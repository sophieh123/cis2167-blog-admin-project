<head>
    <link rel="stylesheet" href="/css/app.css" />
</head>
<nav>
    <ul>
        <li class="first"><a href="/admin/roles/create">Create a new role</a></li>
        <li class="second"><a href="/admin/permissions/create">Create a new permission</a></li>
        <li class="third"><a href="/admin/users">See all users</a></li>
    </ul>
</nav>
