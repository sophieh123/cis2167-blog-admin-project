<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Categories</title>
    <link rel="stylesheet" href="/css/app.css" />
</head>
<body>
<h1>Categories</h1>

<section>

  @if (isset ($categories))

       <ul>
           @foreach ($categories as $category)
               <li>{{ $category->title }}</li>
           @endforeach
       </ul>
   @else
       <p> no categories added yet </p>
   @endif
</section>

{{ Form::open(array('action' => 'CategoryController@create', 'method' => 'get')) }}
  <div class="row">
    {!! Form::submit('Add Category', ['class' => 'button']) !!}
  </div>
{{ Form::close() }}

</body>
</html>
