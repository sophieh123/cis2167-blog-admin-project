<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{

  protected $fillable = [
    'name',
    'label'
  ];
    public function permissions() {
      return $this->belongsToMany(Permissions::class);
    }

    public function givePermissionTo(Permission $permission) {
      return $this->permission()->sync($permission);
    }
}
